#!/bin/bash

#bootstrap script for infrastructure Design Ubuntu 14.04 LTS server

# Can take a single param to allow a specific branch to be installed
HOSTNAME=`cat /etc/hostname`;

# TO BE RUN AS ROOT
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
  echo "This script must be run as root"
  exit
fi

# UPGRADE ALL CURRENT PACKAGES
apt-get update -y && apt-get dist-upgrade -y
#apt-get install autoconf bison build-essential libssl-dev libyaml-dev libreadline6 libreadline6-dev zlib1g zlib1g-dev -y

# INSTALL GIT
apt-get install git -y

# INSTALL RUBY
#git clone https://github.com/sstephenson/rbenv.git ~/.rbenv

# INSTALL PUPPET and r10k
apt-get install puppet -y
gem install r10k

# Clone puppet repository
cd /tmp
rm -rf puppet
git clone https://gitlab.msu.edu/itservicesapps/base-server.git puppet
cd puppet

git fetch
git checkout $HOSTNAME

# INSTALL MODULES
r10k puppetfile install

# RUN MANIFEST
puppet apply manifests/site.pp --modulepath=modules/
#shutdown -r now
